package eu.sshopencloud.marketplace.model.licenses;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "license_types")
@Data
@NoArgsConstructor
public class LicenseType {

    @Id
    private String code;

    @Basic
    @Column(nullable = false)
    private Integer ord;

    @Basic
    @Column(nullable = false)
    private String label;

}
