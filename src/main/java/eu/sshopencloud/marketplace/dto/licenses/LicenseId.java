package eu.sshopencloud.marketplace.dto.licenses;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LicenseId {

    protected String code;

}
