package eu.sshopencloud.marketplace.dto.items;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemRelationId {

    private String code;

}
